`import Ember from 'ember'`

compileMarkdown = (value) ->
  if value
    Ember.String.htmlSafe marked value
  else

CompileMarkdownHelper = Ember.Handlebars.makeBoundHelper compileMarkdown

`export { compileMarkdown }`

`export default CompileMarkdownHelper`
