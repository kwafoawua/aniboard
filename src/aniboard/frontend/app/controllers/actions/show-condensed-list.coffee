`import Ember from 'ember'`
`import DS from 'ember-data'`

ActionsShowCondensedListController = Ember.Controller.extend
  shows: Ember.computed 'model', ->
    DS.PromiseArray.create()

  actions:
    loadShows: ->
      promise = @store.find 'userShowAction',
        user: @get 'model.username'
        since: moment().startOf('model.day').subtract(6, 'days').format()
        limit: 5

      @set 'shows.promise', promise

`export default ActionsShowCondensedListController`
