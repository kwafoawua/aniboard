from __future__ import absolute_import

from django.db import models as django_models, transaction
from django.core import management
from django.conf import settings
from django.utils import timezone

import re
import os
import bs4
import pytz
import celery
import datetime
import requests
import itertools
import subprocess
from celery.utils import log

from . import backend, models, utils, importer

logger = log.get_task_logger(__name__)


@celery.shared_task
def update_last_aired_episodes():
    now = timezone.now()

    for airing_datetime in models.AiringDatetime.objects.for_datetime(now):
        with transaction.atomic():
            # we do this to avoid another task trying to make changes on the
            # ep count, airing datetimes or whatever while we update it
            show_source = models.ShowSource.objects.select_for_update().get(
                id=airing_datetime.show_source_id,
            )

            # update last aired episode
            show_source.last_aired_episode += 1
            show_source.save()

            # marke the airing datetime as used
            airing_datetime.used = True
            airing_datetime.save()


@celery.shared_task
def update_airing_datetimes():
    # generate next week start of the week
    # we generate next week from the end of the current one (it actually is the
    # week after next, but whatever)
    today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    offset = 14 - today.weekday()
    next_week = today + datetime.timedelta(days=offset)
    following_week = next_week + datetime.timedelta(days=7)

    with transaction.atomic():
        # make sure we are the only ones modifying the show sources to avoid
        # race conditions with other tasks that relies on airing datetimes
        for show_source in models.ShowSource.objects.select_for_update(
            ).filter(
                django_models.Q(show__number_episodes__isnull=True) |
                django_models.Q(
                    last_aired_episode__lt=django_models.F(
                        'show__number_episodes'
                    )
                ),
                starts_on__lte=next_week
                ).prefetch_related('schedules'):
            # loop through shows which are still airing
            # check if there are some dates created *after* next week
            force_one = not show_source.airing_datetimes.filter(
                airing_datetime__gte=following_week
            ).exists()

            # generate the airing datetimes
            show_source.create_airing_datetimes(
                offset=2,
                weeks=1,
                force_one=force_one,
            )


@celery.shared_task
def update_finished_airing_shows():
    """ If at least one show source finished airing, mark the show as
    non-airing.
    """
    for show in models.Show.objects.filter(
        show_sources__last_aired_episode=django_models.F('number_episodes')
    ).exclude(
        show_sources__last_aired_episode__lt=django_models.F('number_episodes')
    ).distinct():
        show.airing = False
        show.save()


@celery.shared_task
def delete_finished_airing_shows():
    """ Delete shows that have finished airing at least 2 weeks ago """
    grace_period = timezone.now().replace(
        hour=0, minute=0, second=0
    ) - datetime.timedelta(days=20)

    for show in models.Show.objects.filter(airing=False):
        for show_source in show.show_sources.all():
            last = show_source.airing_datetimes.last()

            if last and last.airing_datetime <= grace_period:
                show.delete()
                break


@celery.shared_task(ignore_result=True)
def create_per_backend_tasks(user_show_id, method_name, *args, **kwargs):
    user_show = models.UserShow.objects.get(id=user_show_id)

    logger.info('Creating "{}" tasks on {} for {}'.format(
        method_name, str(user_show), str(user_show.user)))

    for b in models.Backend.objects.all():
        if user_show.show_source.show.api_data.filter(backend=b).exists() \
           and user_show.user.api_data.filter(backend=b).exists():
            call_backend_api.delay(
                user_show_id, b.backend_name, method_name, *args, **kwargs
            )


@celery.shared_task(bind=True, default_retry_delay=30 * 60, ignore_result=True)
def call_backend_api(self, user_show_id, backend_name, method_name, *args,
                     **kwargs):
    user_show = models.UserShow.objects.get(id=user_show_id)
    b = backend.create_backend(backend_name, user_show.user)

    logger.info('Calling {} on {} for {} in behalf of {}'.format(
        method_name, backend_name, str(user_show), str(user_show.user)))

    try:
        getattr(b, method_name)(user_show, *args, **kwargs)
    except backend.APIException as e:
        raise self.retry(exc=e)


@celery.shared_task(ignore_result=True)
def scrape_season():
    # paths
    base_path = os.path.join(
        settings.BASE_DIR, 'aniboard', 'core', 'importer', 'season-scrape'
    )
    merged_path = os.path.join(base_path, 'merge.json')

    # call the scraper
    process = subprocess.Popen(['node', 'main.js', 'full'], cwd=base_path)
    rc = process.wait()

    # throw an error if it failed to complete
    if rc != 0:
        raise RuntimeError('Failed to execute season scrape script')

    # open the final file
    with open(merged_path, 'r') as merged:
        parser = importer.JSONShowsParser()
        added, updated, errors, fails = parser.parse(merged)

    # send email
    attachements = [
        (os.path.join(base_path, attachment[0]), attachment[0], attachment[1])
        for attachment in (('merge.json', 'application/javascript'),
                           ('syoboi.json', 'application/javascript'),
                           ('livechart.json', 'application/javascript'),
                           ('updates.json', 'application/javascript'))
    ]

    utils.send_mail(
        'Season Scraped',
        'core/email/season_scraped.txt',
        [manager[1] for manager in settings.MANAGERS],
        context={
            'added': added,
            'updated': updated,
            'errors': errors,
            'not_matched': fails
        },
        attachments=attachements
    )


@celery.shared_task(ignore_result=True)
def update_missing_episode_count():
    # we use hummingbird to find ep counts
    backend_name = 'hummingbird'

    backend.load_backend_apis()
    hb_backend = backend.backend_apis[backend_name]()

    # look for all shows with a hummingbird api_data
    updateable_shows = models.Show.objects.filter(
        autoupdate_number_episodes=True,
        api_data__backend__backend_name=backend_name
    )

    # for each one, make a request to hummingbird to check if the ep count got
    # updated
    updated_shows = []

    for show in updateable_shows:
        try:
            api_data = show.api_data.get(backend__backend_name=backend_name)
            result = hb_backend.find_show(api_data.api_id)
            episode_count = result['episode_count']

            if episode_count and episode_count != show.number_episodes:
                show.number_episodes = episode_count
                show.save()

                # add the show to updated
                updated_shows.append(show)
        except (backend.APIException, models.ShowAPIData.DoesNotExist):
            # nothing to do here for now
            pass

    if len(updated_shows) > 0:
        # send email to manager informing of the changes
        utils.send_mail(
            'Episodes count updated',
            'core/email/updated_episode_counts.txt',
            [manager[1] for manager in settings.MANAGERS],
            context={'shows': updated_shows}
        )


@celery.shared_task(ignore_result=True)
def find_breaks():
    all_shows = {}

    syoboi = requests.get('http://cal.syoboi.jp/').text
    soup = bs4.BeautifulSoup(syoboi, 'lxml')

    # find all the bits that mention a break or recopilation
    bits = itertools.chain(
        soup.find_all(class_='v3notice', text=re.compile('放送休止')),
        soup.find_all(class_='v3cmnt', text=re.compile('総集編'))
    )

    for bit in bits:
        # go up, find the title and channel
        container = bit.parent.parent

        syoboi_id_url = container.find(class_='v3title').get('href')
        syoboi_id = re.compile('/tid/(\d+?)#\d+').match(syoboi_id_url).group(1)
        channel = container.find(class_='v3ch').text

        try:
            # compare it against our shows and check it's one of our sources
            matched_show = models.Show.objects.get(syoboi_id=syoboi_id)
            matched_show_source = matched_show.show_sources.get(
                source__name_ja__iexact=channel
            )
        except django_models.ObjectDoesNotExist:
            # show or show source not found, skip it
            pass
        else:
            # we use a list of show sources in case there's more than one
            show_sources = [matched_show_source]

            if matched_show_source.is_premiere:
                # if it's a premiere, we asume simulcasts are updated too
                show_sources += list(
                    matched_show.show_sources.filter(source__is_simulcast=True)
                )

            # add the show and show sources for the report
            if matched_show not in all_shows:
                all_shows[matched_show] = show_sources
            else:
                all_shows[matched_show] += show_sources

            # proceed to mark skip date of the next episode on the show sources
            next_episode = int(container.find(class_='count').text[1:]) + 1

            for show_source in show_sources:
                episode_diff = next_episode - show_source.last_aired_episode

                # find the airing datetime for the last aired episode
                last_used_datetime = show_source.airing_datetimes.filter(
                    used=True
                ).latest()

                # find the datetime to skip based on the current one
                next_datetimes = show_source.airing_datetimes.filter(
                    airing_datetime__gt=last_used_datetime.airing_datetime
                )

                if episode_diff > 0 and next_datetimes.count() >= episode_diff:
                    to_skip = next_datetimes[episode_diff - 1]
                    to_skip.skip = True
                    to_skip.save()

                    # make sure there is at least one datetime after the skip
                    if episode_diff == len(next_datetimes):
                        show_source.create_airing_datetimes()

    if len(all_shows) > 0:
        # send an email to managers for them to check if the changes where
        # correct
        utils.send_mail(
            'Breaks report',
            'core/email/breaks_report.txt',
            [manager[1] for manager in settings.MANAGERS],
            context={'shows': all_shows}
        )


@celery.shared_task(ignore_result=True)
def update_syoboi():
    updated_shows = []
    failed_shows = []

    # pick up the syoboi page 6 days from today to look for changes
    today = timezone.now().date()
    check_date = (today + datetime.timedelta(days=6)).strftime('%Y/%m/%d')
    url = 'http://cal.syoboi.jp/'
    headers = {'cookie': 'TimeGraph=0'}
    params = {'date': check_date}

    r = requests.get(url, params=params, headers=headers)
    syoboi = r.text
    soup = bs4.BeautifulSoup(syoboi, 'lxml')

    changes = soup.find_all(class_='offset')

    for change in changes:
        # for each change, find relevant info
        container = change.parent.parent.parent

        syoboi_id_url = container.find(class_='tdTitle').find('a').get('href')
        syoboi_id = re.compile('/tid/(\d+?)#\d+').match(syoboi_id_url).group(1)
        source = container.find(class_='tdCh').find('a').text
        date_time = container.find(class_='timeDetail').text.split(',')[0]
        date_time = timezone.make_aware(
            datetime.datetime.fromtimestamp(float(date_time)), pytz.UTC
        )
        # date = date_time.strftime('%Y-%m-%d')
        # time = date_time.strftime('%H:%M')
        # print(syoboi_id, source, date, time)

        offset_re = re.compile(u'(↑|↓)(\d+)').match(change.text)
        offset_type = offset_re.group(1)
        offset_amt = int(offset_re.group(2))
        if re.match(u'(↓)', offset_type) is not None:
            offset_amt = -offset_amt

        original_datetime = date_time + datetime.timedelta(minutes=offset_amt)
        # print(offset_type, offset_amt, date_time, original_datetime)

        try:
            # look for the show that had a change
            show_source = models.ShowSource.objects.get(
                show__syoboi_id=syoboi_id, source__name_ja__iexact=source
            )
        except models.ShowSource.DoesNotExist:
            # show not found, skip
            logger.info(
                '{} show source for {} not found, skipping'.format(
                    syoboi_id, source
                )
            )
        else:
            # put together a register for the found show
            changed_show = {
                'show_source': show_source,
                'original_datetime': original_datetime,
                'new_datetime': date_time
            }

            # show found, get datetime
            airing_datetime = show_source.airing_datetimes.for_datetime(
                original_datetime, use_hour=True, use_minute=True
            ).first()

            if airing_datetime:
                # update the datetime
                airing_datetime.airing_datetime = date_time
                airing_datetime.save()

                updated_shows.append(changed_show)
            else:
                failed_shows.append(changed_show)

    if updated_shows or failed_shows:
        # if we matched some shows, send an email
        utils.send_mail(
            'Timeslot update report',
            'core/email/timeslot_update_report.txt',
            [manager[1] for manager in settings.MANAGERS],
            context={
                'updated_shows': updated_shows,
                'failed_shows': failed_shows
            }
        )


@celery.shared_task(ignore_result=True)
def delete_user_traces():
    models.UserTrace.objects.all().delete()


@celery.shared_task(ignore_result=True)
def clean_old_images():
    management.call_command('deleteorphaned')


@celery.shared_task(ignore_result=True)
def backup_database():
    management.call_command('dbbackup')
