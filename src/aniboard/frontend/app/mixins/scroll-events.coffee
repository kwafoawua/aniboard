`import Ember from 'ember'`

ScrollEventsMixin = Ember.Mixin.create
  bind: false

  bindScrolling: (opts={debounce: 100}) ->
    if opts.debounce? and opts.debounce
      @onScroll = =>
        Ember.run.debounce @, @scrolled, opts.debounce
    else
      @onScroll = =>
        Ember.run @scrolled()

    $(document).on 'touchmove', @onScroll
    $(window).on 'scroll', @onScroll
    @set 'bind', true

  unbindScrolling: ->
    $(window).off 'scroll', @onScroll
    $(document).off 'touchmove', @onScroll
    @set 'bind', false

`export default ScrollEventsMixin`
