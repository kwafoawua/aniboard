`import Ember from 'ember'`
`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'user-show-comment', 'UserShowComment', {
  # Specify the other units that are required for this test.
  needs: [
    'model:user'
    'model:user-show'
    'model:show'
    'model:source'
    'model:show-source'
    'service:validations'
    'ember-validations@validator:local/presence'
    'ember-validations@validator:local/length'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model

test 'disliked updates correctly', (assert) ->
  assert.expect 3

  comment = @subject liked: true
  assert.equal comment.get('disliked'), false,
    'If liked is true, disliked is false'

  Ember.run ->
    comment.set 'liked', false
  assert.equal comment.get('disliked'), true,
    'If liked is false, disliked is true'

  Ember.run ->
    comment.set 'liked', null
  assert.equal comment.get('disliked'), false, 'If we don\'t know, asume false'
