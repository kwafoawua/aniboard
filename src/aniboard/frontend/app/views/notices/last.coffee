`import Ember from 'ember'`

NoticesLastView = Ember.View.extend
  elementId: 'notices'
  classNameBindings: ['open']

  open: Ember.computed 'controller.last.isRead', ->
    last = @get 'controller.last'
    last and not last.get('isRead')

`export default NoticesLastView`
