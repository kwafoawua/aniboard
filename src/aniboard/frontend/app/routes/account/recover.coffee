`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountRecoverRoute = Ember.Route.extend DialogRouteMixin, UnauthenticatedRouteMixin,
  titleToken: 'Recover Password'

  model: (params) ->
    @store.createRecord 'passwordRecovery',
      recoveryCode: params['recovery_code']

`export default AccountRecoverRoute`
