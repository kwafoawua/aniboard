from django.conf import settings
from django.utils import timezone
import requests
import xmltodict
from xml.etree import ElementTree as ET
from . import BaseAPI, APIException, APIUnauthorizedException

API_URL = 'https://myanimelist.net/api/{}.xml'
API_HEADERS = {
    'user-agent': settings.MAL_USER_AGENT,
}


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'watching',
        BaseAPI.BASE_STATUS.COMPLETED: 'completed',
        BaseAPI.BASE_STATUS.ONHOLD: 'onhold',
        BaseAPI.BASE_STATUS.DROPPED: 'dropped',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'plantowatch',
    }

    def __init__(self, username=None, password=None):
        self._username = username
        self._password = password

    def _make_entry(self, params):
        root = ET.Element('entry')

        for param, value in params.items():
            element = ET.SubElement(root, param)
            element.text = str(value)

        return ET.tostring(root).decode('utf-8')

    def _api_call(self, entrypoint, params=None, method='get',
                  success_string=None):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param params: parameters to include in the request data.
        '''
        url = API_URL.format(entrypoint)

        # make the data entry
        if params:
            data = {'data': self._make_entry(params)}
        else:
            data = None

        # call the api
        request_call = getattr(requests, method)

        try:
            response = request_call(
                url,
                data=data,
                auth=(self._username, self._password),
                headers=API_HEADERS,
            )
            response.raise_for_status()
        except requests.RequestException as e:
            text = '{} - {}'.format(str(e), e.response.text)

            if e.response.status_code == requests.codes.UNAUTHORIZED:
                raise APIUnauthorizedException(text)
            else:
                raise APIException(text)

        result = response.text

        if success_string:
            if result.find(success_string) == -1:
                raise APIException(result)
        else:
            result = xmltodict.parse(result)

        return result

    def authenticate(self, **credentials):
        self._username = credentials['username']
        self._password = credentials['password']

        try:
            self._api_call('account/verify_credentials')
        except APIException as e:
            self._username = None
            self._password = None
            raise e

        return credentials

    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        data = {'status': self.get_status(status)}

        if status == BaseAPI.BASE_STATUS.COMPLETED:
            # if completed, add ending date
            data['date_finish'] = timezone.now().strftime('%m%d%Y')

        if episodes_watched is not None:
            data['episode'] = episodes_watched

        if rating is not None:
            data['score'] = rating

        self._api_call(
            'animelist/update/{}'.format(id),
            data,
            'post',
            'Updated'
        )

    def add_show(self, episodes_watched=None, status=None, id=None):
        data = {
            'episode': episodes_watched,
            'status': self.get_status(status),
            'date_start': timezone.now().strftime('%m%d%Y')
        }

        self._api_call(
            'animelist/add/{}'.format(id),
            data,
            'post',
            'Created'
        )

    def remove_show(self, id=None):
        self._api_call(
            'animelist/delete/{}'.format(id),
            method='delete',
            success_string='Deleted'
        )
