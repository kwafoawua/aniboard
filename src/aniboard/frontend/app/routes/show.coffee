`import Ember from 'ember'`

ShowRoute = Ember.Route.extend
  titleToken: ->
    model = @modelFor @routeName
    model.get 'name'

  model: (params) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      @store.find('show', params).then((result) ->
        if result and result.get('length') is 1
          resolve result.get 'firstObject'
        else
          reject 'Show not found'
      ).catch (error) ->
        reject error

  serialize: (model, params) ->
    slug: model.get 'slug'

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'shows'

`export default ShowRoute`
