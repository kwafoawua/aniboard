`import Ember from 'ember'`
`import {THEMES} from '../structures/settings'`

SettingsController = Ember.Controller.extend
  debouncedSave: null
  isUpdating: false
  themes: Ember.computed ->
    themes = []

    for theme of THEMES
      themes.push
        id: theme
        name: theme.charAt(0).toUpperCase() + theme.slice(1)

    themes

  _save: ->
    model = @get 'model'

    @set 'isUpdating', true
    @set 'debouncedSave', null
    @set 'hermes.info', 'Updating settings...'

    model.save().then(=>
      @set 'hermes.success', 'Settings successfully updated.'
    ).catch(=>
      model.rollback()
      @set 'hermes.error', 'Failed to update settings.'
    ).finally =>
      @set 'isUpdating', false

  save: Ember.observer 'model.isDirty', ->
    if @get('model.isDirty') and not @get('isUpdating')
      @set 'debouncedSave', Ember.run.debounce @, @_save, 500
    else if @get('debouncedSave')?
      Ember.run.cancel @get('debouncedSave')
      @set 'debouncedSave', null

`export default SettingsController`
