`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'show-source', 'ShowSource', {
  # Specify the other units that are required for this test.
  needs: [
    'model:show'
    'model:source'
    'model:airing-datetime'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
