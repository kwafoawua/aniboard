`import Ember from 'ember'`

dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',\
            'Saturday', 'Not Airing This Week', 'Recently Finished']

ShowDayListControllerMixin = Ember.Mixin.create
  title: Ember.computed 'model.day', ->
    dayNames[@get('model.day')]

`export default ShowDayListControllerMixin`
