from django.utils import text, timezone
import requests
from aniboard.core import models
from aniboard.core.importer import base


class TextShowsParser(base.BaseParser):
    """ Specialized parser that creates shows and show sources based on text
    files.

    Format:
        romaji title
        jp title
        ep count
        img link
        MAL ID
        HB ID
        AniList ID
        date
        time (UTC)
        source

    "//" is used to ignore lines.
    a blankline represents (together with the field count) the end of the
    entry.
    """
    # fields by order to parse from the file
    fields = [
        'title', 'jp_title', 'ep_count', 'img_link', 'mal_id', 'hb_id',
        'anilist_id', 'date', 'time', 'source'
    ]
    # required fields
    required = ['title']

    def parse_entries(self, import_text):
        """ Parses the file, returning a dictionary with the initial values.
        """
        for number, raw_entry in enumerate(import_text.strip().split('\n\n')):
            try:
                entry = self.parse_entry(raw_entry)
            except base.ParseException as ex:
                entry = {
                    'error': self.build_error(number, str(ex))
                }

            yield entry

    def parse_entry(self, raw_entry):
        fields = raw_entry.strip().split('\n')

        if len(fields) != len(self.fields):
            raise base.ParseException(
                'Has {} fields instead of {}'.format(
                    len(fields), len(self.fields)
                )
            )

        entry = {}

        for i in range(0, len(fields)):
            name = self.fields[i]
            field = fields[i]

            if field.startswith('//'):
                if name in self.required:
                    raise base.ParseException(
                        'Doesn\'t have a {}, which is required'.format(name)
                    )
                entry[name] = None
                continue

            entry[name] = field

        return entry

    def preprocess_entry(self, entry):
        """ Process the initialy parsed entry to clean the values or generate
        new values based on those.
        """
        entry['slug'] = text.slugify(entry['title'])

        if entry['ep_count'] is not None:
            # the ep count should be an integer
            try:
                entry['ep_count'] = int(entry['ep_count'])
            except ValueError:
                raise base.PreprocessException(
                    'The ep count isn\'t an integer'
                )

        # we have to download the image link
        if entry['img_link'] is not None:
            try:
                response = requests.get(entry['img_link'], stream=True)
                response.raw.decode_content
                response.raise_for_status()
                entry['img_content'] = response
            except Exception as ex:
                raise base.PreprocessException(
                    'The image couldn\'t be retrieved: {}'.format(ex)
                )

        if entry['source']:
            # obtain the source from the name
            try:
                entry['source'] = models.Source.objects.get(
                    name=entry['source']
                )
            except models.Source.DoesNotExist:
                raise base.PreprocessException(
                    'The source {} doesn\'t exist'.format(entry['source'])
                )

        if entry['time'] is not None:
            # parse the time if it's present
            try:
                entry['time'] = timezone.datetime.strptime(
                    entry['time'], '%H:%M'
                ).time()
            except ValueError:
                raise base.PreprocessException('The time couldn\'t be parsed')

        if entry['time'] is not None and entry['date'] is not None:
            # parse the date if it's present
            try:
                entry['date'] = timezone.datetime.strptime(
                    entry['date'], '%Y-%m-%d'
                )
            except ValueError:
                raise base.PreprocessException('The date couldn\'t be parsed')

    def _update_image(self, entry, show):
        """ Updates the image for a show. """
        self.update_image(
            entry['img_link'],
            entry['img_content'].raw.read(),
            show
        )

    def _update_api_datas(self, entry, show):
        """ Creates and/or updates the related api data. """
        backends_keys = [
            ('mal_id', 'mal'), ('hb_id', 'hummingbird'),
            ('anilist_id', 'anilist')
        ]
        backends = {}

        for api_key, backend in backends_keys:
            if entry[api_key] is not None:
                backends[backend] = entry[api_key]

        self.update_api_datas(backends, show)

    def create_show(self, entry):
        # initial show
        show = models.Show.objects.create(
            slug=entry['slug'],
            name=entry['title'],
            name_ja=entry['jp_title'],
            number_episodes=entry['ep_count']
        )

        if entry['img_link'] is not None:
            # the image can be missing
            self._update_image(entry, show)

        # show source
        self.update_show_source(entry, show)

        # api datas
        self._update_api_datas(entry, show)

        return show

    def update_show(self, entry, show):
        # update base fields
        if entry['jp_title'] is not None:
            show.name_ja = entry['jp_title']

        if entry['ep_count'] is not None:
            show.number_episodes = entry['ep_count']

        show.save()

        if entry['img_link'] is not None:
            # the image can be missing
            self._update_image(entry, show)

        # update api datas if needed
        self._update_api_datas(entry, show)

        # update show source
        self.update_show_source(entry, show)

    def postprocess_entry(self, entry):
        if entry['img_link'] is not None and entry['img_content'] is not None:
            # we have to make sure the request is closed
            entry['img_content'].close()
