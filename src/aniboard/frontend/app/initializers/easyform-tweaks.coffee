# Takes two parameters: container and app
initialize = () ->
  # This is a little ember easyForm hack to make errors display when the form
  # is submitted
  Ember.EasyForm.Form.reopen
    submit: (event) ->
      if event
        event.preventDefault()

      formForModel = @get 'formForModel'
      controller = @get 'controller'
      action = @get 'action'

      if Ember.isNone formForModel.validate
        controller.send action
      else
        if not Ember.isNone @get('formForModel').validate
          promise = formForModel.validate()
        else
          promise = formForModel.get('model').validate()

        promise.then( =>
          if formForModel.get 'isValid'
            controller.send action
        ).finally =>
          @showErrors()

    showErrors: Ember.observer 'formForModel.showErrors', ->
      formForModel = @get 'formForModel'

      if not formForModel.get 'isValid'
        @get('childViews').forEach (view) ->
          view.focusOut()

EasyformTweaksInitializer =
  name: 'easyform-tweaks'
  initialize: initialize

`export {initialize}`
`export default EasyformTweaksInitializer`
