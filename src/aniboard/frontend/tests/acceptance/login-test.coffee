`import Ember from 'ember'`
`import { module, test } from 'qunit'`
`import startApp from '../helpers/start-app'`

application = null

module 'Acceptance: Login',
  beforeEach: ->
    application = startApp()
    testServerSetup()

    ###
    Don't return as Ember.Application.then is deprecated.
    Newer version of QUnit uses the return value's .then
    function to wait for promises if it exists.
    ###
    return

  afterEach: ->
    testServerTeardown()
    Ember.run application, 'destroy'

test 'page loads', (assert) ->
  assert.expect 2

  visit '/account/login'

  andThen ->
    assert.equal currentPath(), 'landing.account.login',
      'We are on the login page'
    assert.equal find('#login').length, 1, 'Login form is shown'

test 'authentication fails', (assert) ->
  assert.expect 1

  visit '/account/login'
  fillIn 'input[name=identification]', 'invalid_username'
  fillIn 'input[name=password]', 'invalid_password'
  click 'button[type=submit]'

  andThen ->
    assert.ok find('aside.hermes').hasClass('error'), 'Error message is shown'

test 'authentication succeeds', (assert) ->
  assert.expect 2

  visit '/account/login'
  fillIn 'input[name=identification]', 'test'
  fillIn 'input[name=password]', 'test'
  click 'button[type=submit]'

  andThen ->
    assert.ok find('aside.hermes').hasClass('success'),
      'Success message is shown'
    assert.equal currentPath(), 'landing.index',
      'We got redirected to the user shows page'
    invalidateSession()

