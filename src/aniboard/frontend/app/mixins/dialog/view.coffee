`import Ember from 'ember'`
`import FadeViewMixin from '../fade-view'`

DialogViewMixin = Ember.Mixin.create FadeViewMixin,
  tagName: 'aside'
  ariaRole: 'dialog'
  layoutName: 'dialog/layout'
  bodyScroll: null

  close: ->
    @get('controller').send 'closeDialog'

  click: (event) ->
    if event.target is @element
      @close()

  closeOnEscape: (event) ->
    if event.keyCode is 27
      @close()

  didInsertElement: ->
    # listen to the escape key to close down the dialog
    $(document).on 'keydown.dialog', (event) => @closeOnEscape event

    # lock body scroll
    $body = $('body')
    bodyScroll = $(window).scrollTop()
    @set 'bodyScroll', bodyScroll

    $body.addClass 'dialog-open'
    $body.css 'margin-top', -bodyScroll

  willDestroyElement: ->
    # disconnect keydown listener
    $(document).off 'keydown.dialog'

    # unlock body scroll
    bodyScroll = @get 'bodyScroll', bodyScroll
    @set 'bodyScroll', null

    $body = $('body')
    $body.removeClass 'dialog-open'
    $body.css 'margin-top', ''
    $(window).scrollTop bodyScroll

`export default DialogViewMixin`
