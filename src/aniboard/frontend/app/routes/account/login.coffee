`import Ember from 'ember'`
`import DialogRouteMixin from 'aniboard/mixins/dialog/route'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

LoginRoute = Ember.Route.extend DialogRouteMixin, UnauthenticatedRouteMixin,
  titleToken: 'Login'

`export default LoginRoute`
