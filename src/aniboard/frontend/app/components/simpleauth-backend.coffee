`import Ember from 'ember'`
`import EmberValidations from 'ember-validations'`

SimpleauthBackendComponent = Ember.Component.extend EmberValidations.Mixin,
  backend: null
  username: ''
  password: ''
  validations:
    username:
      presence: true
    password:
      presence: true

  actions:
    connect: ->
      username = @get 'username'
      password = @get 'password'

      @sendAction 'action',
        username: username
        password: password

`export default SimpleauthBackendComponent`
