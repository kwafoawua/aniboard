`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

UserShowDropView = Ember.View.extend DialogViewMixin,
  classNames: 'drop'

`export default UserShowDropView`
