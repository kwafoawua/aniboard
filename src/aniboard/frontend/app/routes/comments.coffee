`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

CommentsRoute = Ember.Route.extend AuthenticatedRouteMixin,
  beforeModel: ->
    params = @paramsFor @routeName
    userShow = @modelFor 'userShow'

    if isNaN params.episode
      throw new Ember.Error 'The episode should be a number'

    episode = parseInt params.episode
    lastWatchedEpisode = userShow.get 'lastEpisodeWatched'

    if episode < 1
      throw new Ember.Error 'Invalid episode number'
    else if episode > lastWatchedEpisode
      throw new Ember.Error 'You haven\'t watched this episode yet'

    # set the title
    @set 'titleToken', "Episode #{episode}"

  actions:
    closeDialog: ->
      # we take care of it ourselves
      @transitionTo 'userShows'

`export default CommentsRoute`
