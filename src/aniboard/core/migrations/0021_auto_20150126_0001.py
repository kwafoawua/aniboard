# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_settings_ask_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usershowcomment',
            name='liked',
            field=models.NullBooleanField(),
            preserve_default=True,
        ),
    ]
