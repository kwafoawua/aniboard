`import { test, moduleFor } from 'ember-qunit'`
`import withEventHub from 'aniboard/tests/helpers/with-event-hub'`

moduleFor 'controller:user-shows', 'UserShowsController', {
  # Specify the other units that are required for this test.
  # needs: ['controller:foo']
  subject: withEventHub
}

# Replace this with your real tests.
test 'it exists', ->
  controller = @subject()
  ok controller
