`import Ember from 'ember'`
`import DS from 'ember-data'`

UserShowAction = DS.Model.extend
  userShow: DS.belongsTo 'user-show'
  comment: DS.belongsTo 'user-show-comment'
  episode: DS.attr 'number'
  actionDatetime: DS.attr 'date'

  actionMoment: Ember.computed 'actionDatetime', ->
    moment @get 'actionDatetime'

  # user show aliases
  name: Ember.computed.alias 'userShow.name'
  coverImage: Ember.computed.alias 'userShow.coverImage'

`export default UserShowAction`
