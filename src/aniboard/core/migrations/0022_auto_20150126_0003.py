# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20150126_0001'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='neutral_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='showepisode',
            name='neutral_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='usershow',
            name='neutral_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
    ]
