`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

UserShowRateRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Rate'

  setupController: (controller, model) ->
    @_super controller, model
    rating = model.get 'rating'
    score = model.get 'score'

    if not rating? and score
      rating = Math.floor score / 10

      # set a default rating
      model.set 'rating', rating

  actions:
    closeDialog: ->
      userShow = @modelFor 'userShow'

      if userShow.get('isDirty')
        # rollback the default rating in case it doesn't get saved
        userShow.rollback()

      return true

`export default UserShowRateRoute`
