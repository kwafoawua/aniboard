`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

ShowAddView = Ember.View.extend DialogViewMixin,
  classNames: ['add']

`export default ShowAddView`
