const request = require('request');
const cheerio = require('cheerio');
const moment = require('moment');

module.exports = (show_sources, cb) => {
  console.log('Connecting to Syoboi...');
  let update = {
    'updated': [],
    'not_updated': []
  };
  request({
    url: 'http://cal.syoboi.jp/',
    qs: {
      date: moment().utcOffset('+09:00').format('YYYY/MM/DD')
    },
    headers: {
      Cookie: 'TimeGraph=0; ViewDays=7;'
    }
  }, (err, res, body) => {
    console.log('Retrieved from Syoboi!');
    let $ = cheerio.load(body);
    
    let today = $('.Today').text();
    console.log(today);
    
    cb(update);
  })
}
