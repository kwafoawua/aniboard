[program:django]
command={{ virtualenv_path }}/bin/gunicorn aniboard.wsgi:application -c gunicorn.py
environment=DJANGO_SETTINGS_MODULE="aniboard.settings",LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
stdout_logfile={{ log_path }}/{{ project_name }}.log
stderr_logfile={{ log_path }}/{{ project_name }}.log
user=www-data
autostart=true
autorestart=unexpected

[program:celery]
command={{ virtualenv_path }}/bin/celery -A aniboard worker -n aniboard -Q celery --loglevel=INFO
environment=LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
user=www-data
numprocs=1
stdout_logfile={{ log_path }}/{{ project_name }}-celery.log
stderr_logfile={{ log_path }}/{{ project_name }}-celery.log
autostart=true
autorestart=unexpected
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

[program:celery_sync]
command={{ virtualenv_path }}/bin/celery -A aniboard worker -n aniboard_sync -Q sync --loglevel=INFO
environment=LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
user=www-data
numprocs=1
stdout_logfile={{ log_path }}/{{ project_name }}-celery_sync.log
stderr_logfile={{ log_path }}/{{ project_name }}-celery_sync.log
autostart=true
autorestart=unexpected
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

[program:celery_shows]
command={{ virtualenv_path }}/bin/celery -A aniboard worker -n aniboard_shows -Q shows --loglevel=INFO
environment=LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
user=www-data
numprocs=1
stdout_logfile={{ log_path }}/{{ project_name }}-celery_shows.log
stderr_logfile={{ log_path }}/{{ project_name }}-celery_shows.log
autostart=true
autorestart=unexpected
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

[program:celerybeat]
command={{ virtualenv_path }}/bin/celery -A aniboard beat --schedule={{ log_path }}/aniboard-celerybeat-schedule --loglevel=INFO
environment=LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
user=www-data
numprocs=1
stdout_logfile={{ log_path }}/{{ project_name }}-celerybeat.log
stderr_logfile={{ log_path }}/{{ project_name }}-celerybeat.log
autostart=true
autorestart=unexpected
startsecs=10

[program:celery_flower]
command={{ virtualenv_path }}/bin/celery -A aniboard flower --basic_auth="{{ flower_user }}:{{ flower_password }}"
environment=LANG="en_US.UTF-8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"
directory={{ django_path }}
user=www-data
numprocs=1
stdout_logfile={{ log_path }}/{{ project_name }}-celery_flower.log
stderr_logfile={{ log_path }}/{{ project_name }}-celery_flower.log
autostart=true
autorestart=unexpected

[group:aniboard]
programs=django,celery,celery_sync,celery_shows,celerybeat,celery_flower
