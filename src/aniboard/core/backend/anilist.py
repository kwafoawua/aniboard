from django.conf import settings
import requests
import datetime
import logging
from . import BaseAPI, APIException, APIUnauthorizedException

LOGGER = logging.getLogger(__name__)
API_URL = 'https://anilist.co/api/{}'


class API(BaseAPI):
    status_mapping = {
        BaseAPI.BASE_STATUS.WATCHING: 'watching',
        BaseAPI.BASE_STATUS.COMPLETED: 'completed',
        BaseAPI.BASE_STATUS.ONHOLD: 'on-hold',
        BaseAPI.BASE_STATUS.DROPPED: 'dropped',
        BaseAPI.BASE_STATUS.PLANTOWATCH: 'plan to watch',
    }

    def __init__(self, **api_data):
        self._assign_token(api_data)

    def _assign_token(self, data):
        if 'access_token' in data:
            self._access_token = data['access_token']
            self._expires = datetime.datetime.utcfromtimestamp(
                data['expires']
            )

            if 'refresh_token' in data:
                self._refresh_token = data['refresh_token']
        else:
            self._access_token = None
            self._expires = None
            self._refresh_access_token = None

    @property
    def authentication_expired(self):
        return datetime.datetime.utcnow() >= self._expires

    def refresh_authentication(self):
        params = {
            'grant_type': 'refresh_token',
            'client_id': settings.ANILIST_CLIENT_ID,
            'client_secret': settings.ANILIST_CLIENT_SECRET,
            'refresh_token': self._refresh_token,
        }
        response = self._api_call(
            'auth/access_token',
            params,
            'post'
        )
        self._assign_token(response)

        return response

    def _api_call(self, entrypoint, data=None, method='get'):
        '''Makes a call against the api.
        :param entrypoint: API method to call.
        :param data: data to include in the request.
        '''
        url = API_URL.format(entrypoint)
        auth_request = 'auth' in entrypoint

        request_params = {}

        if auth_request:
            # auth request use url params for some reason
            request_params['params'] = data
        else:
            # every request except for auth requests needs these headers
            # and uses encoded data instead of url params
            request_params['headers'] = {
                'Authorization': 'Bearer {}'.format(self._access_token),
            }
            request_params['data'] = data

        # call the api
        request_call = getattr(requests, method)

        try:
            LOGGER.debug('Calling api: %s, %s', url, request_params)

            response = request_call(
                url,
                **request_params
            )

            LOGGER.debug('Response: %s', response.text)

            response.raise_for_status()
        except requests.RequestException as e:
            text = '{} - {}'.format(str(e), e.response.text)

            if e.response.status_code == requests.codes.UNAUTHORIZED or \
                e.response.status_code == requests.codes.BAD_REQUEST \
                    and e.response.json()['error_description'] \
                    == 'The refresh token is invalid.':
                raise APIUnauthorizedException(text)
            else:
                raise APIException(text)

        try:
            json_response = response.json()

            if 'error' in json_response:
                # raise error
                raise APIException(json_response['error_message'])
        except (ValueError, TypeError):
            json_response = None

        return json_response

    def authenticate(self, **credentials):
        if not self._access_token:
            credentials.update({
                'grant_type': 'authorization_code',
                'client_id': settings.ANILIST_CLIENT_ID,
                'client_secret': settings.ANILIST_CLIENT_SECRET,
                'redirect_uri': settings.ANILIST_REDIRECT_URI,
            })
            response = self._api_call(
                'auth/access_token',
                credentials,
                'post'
            )
            self._assign_token(response)

        return response

    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        data = {'id': id, 'list_status': self.get_status(status)}

        if episodes_watched is not None:
            data['episodes_watched'] = episodes_watched

        if rating is not None:
            # rating is based on 5 on hummingbird
            data['score'] = rating

        self._api_call(
            'animelist',
            data,
            'put'
        )

    def add_show(self, episodes_watched=None, status=None, id=None):
        data = {'id': id, 'list_status': self.get_status(status)}

        if episodes_watched is not None:
            data['episodes_watched'] = episodes_watched

        self._api_call(
            'animelist',
            data,
            'post'
        )

    def remove_show(self, id=None):
        self._api_call('animelist/{}'.format(id), method='delete')
