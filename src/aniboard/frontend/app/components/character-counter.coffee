`import Ember from 'ember'`

CharacterCounterComponent = Ember.Component.extend
  classNameBindings: ['over']
  value: null
  max: null

  counter: Ember.computed 'value', 'max', ->
    value = @get 'value'
    length = if value? then value.length else 0
    max = @get 'max'

    max - length

  over: Ember.computed.lt 'counter', 0

`export default CharacterCounterComponent`
