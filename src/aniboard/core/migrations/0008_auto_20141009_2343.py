# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_airingdatetime_used'),
    ]

    operations = [
        migrations.AlterField(
            model_name='airingdatetime',
            name='used',
            field=models.BooleanField(default=False),
        ),
    ]
