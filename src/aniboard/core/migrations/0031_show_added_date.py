# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_show_autoupdate_number_episodes'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='added_date',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2015, 12, 26, 16, 46, 45, 636011, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
