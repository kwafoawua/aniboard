`import ApplicationSerializer from './application'`

ShowEpisodeSerializer = ApplicationSerializer.extend
  attrs:
    show:
      serialize: 'ids'
      deserialize: 'ids'

`export default ShowEpisodeSerializer`
