# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20140927_0018'),
    ]

    operations = [
        migrations.AddField(
            model_name='airingdatetime',
            name='used',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
