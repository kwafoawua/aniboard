`import Ember from 'ember'`
`import { module, test } from 'qunit'`
`import startApp from '../helpers/start-app'`

application = null

module 'Acceptance: UserShow',
  beforeEach: ->
    application = startApp()
    testServerSetup()
    login identification: 'test', password: 'test'
    ###
    Don't return as Ember.Application.then is deprecated.
    Newer version of QUnit uses the return value's .then
    function to wait for promises if it exists.
    ###
    return

  afterEach: ->
    invalidateSession().then ->
      testServerTeardown()
      Ember.run application, 'destroy'

test 'page loads', (assert) ->
  assert.expect 2

  visit '/watching'

  andThen ->
    assert.equal currentPath(), 'userShows.index',
      'We are on the user show index page'
    assert.equal find('.week_list').length, 1,
      'The weekly list of anime is shown'

test 'we don\'t have any shows yet, so a message is shown', (assert) ->
  assert.expect 1

  visit '/watching'

  andThen ->
    assert.equal find('.empty').length, 1,
      'We see the message indicating we don\'t have any shows yet'

test 'we can add a show to our list', (assert) ->
  assert.expect 11
  showName = null

  visit '/watching'

  andThen ->
    assert.equal find('.empty .shows-link').length, 1,
      'We see the link to the add shows page'

  click '.empty .shows-link'

  andThen ->
    assert.equal currentPath(), 'shows.index', 'We are on the add shows page'
    assert.equal find('.add_show').length, 1, 'We see the shows list'
    assert.equal find('.show').length, 1, 'We see a show we can add'
    showName = find('.show .title h3').text()

  click '.show a'

  andThen ->
    assert.equal currentPath(), 'shows.show.add', 'We are on the add show page'
    assert.equal find('.add').length, 1, 'We see the add dialog'

  fillIn '.add .lastEpisodeWatched input', 0
  fillIn '.add .showSource select', 1
  click '.add button[type=submit]'

  andThen ->
    assert.ok find('aside.hermes').hasClass('success'),
      'We see the success message'
    assert.equal currentPath(), 'shows.index', 'We are back on shows list'
    assert.equal find('.show').length, 0,
      'We see the show dissapear from the list'

  visit '/watching'

  andThen ->
    assert.equal find('.show').length, 1, 'We see a new show'
    assert.equal find('.show .title h3').text(), showName,
      'We make sure it\'s the same we added before'

test 'we can watch an episode of a show on our list', (assert) ->
  assert.expect 4

  # add a show
  visit '/shows'
  click '.show a'
  fillIn '.add .lastEpisodeWatched input', 0
  fillIn '.add .showSource select', 1
  click '.add button[type=submit]'

  visit '/watching'

  andThen ->
    assert.equal find('.show').length, 1,
      'We see our show on the list'
    assert.equal find('.ep_wat').text(), '0',
      'We haven\'t watched any episode of the show yet'

  click '.show button'

  andThen ->
    assert.ok find('aside.hermes').hasClass('success'),
      'We see a success message'
    assert.equal find('.ep_wat').text(), '1',
      'We see the episode counter update'

test 'we can update the watched count of the episode', (assert) ->
  assert.expect 8

  # add a show
  visit '/shows'
  click '.show a'
  fillIn '.add .lastEpisodeWatched input', 0
  fillIn '.add .showSource select', 1
  click '.add button[type=submit]'

  visit '/watching'

  # watch an ep
  click '.show button'

  andThen ->
    assert.equal find('.show').length, 1,
      'We see our show on the list'
    assert.equal find('.ep_wat').text(), '1',
      'We have already watched an episode of the show'

  click '.show .edit-button'

  andThen ->
    assert.equal currentPath(), 'userShows.userShow.edit',
      'We are on the show edit'
    assert.equal find('.edit').length, 1, 'We see the edit dialog'
    assert.equal find('.edit .lastEpisodeWatched input').length, 1,
      'We see the episode edit field'

  fillIn '.lastEpisodeWatched input', 0
  click '.edit button[type=submit]'

  andThen ->
    assert.ok find('aside.hermes').hasClass('success'),
      'We see the success message'
    assert.equal currentPath(), 'userShows.index', 'We are back on the list'
    assert.equal find('.ep_wat').text(), '0',
      'We see the episode counter update'


test 'we can drop a show from our list', (assert) ->
  assert.expect 8

  # add a show
  visit '/shows'
  click '.show a'
  fillIn '.add .lastEpisodeWatched input', 0
  fillIn '.add .showSource select', 1
  click '.add button[type=submit]'

  visit '/watching'

  andThen ->
    assert.equal find('.show').length, 1,
      'We see our show on the list'

  click '.show .edit-button'

  andThen ->
    assert.equal currentPath(), 'userShows.userShow.edit',
      'We are on the show edit'
    assert.equal find('a.drop').length, 1, 'We see drop button'

  click 'a.drop'

  andThen ->
    assert.equal currentPath(), 'userShows.userShow.drop',
      'We are on the show drop'
    assert.equal find('.drop').length, 1, 'We see drop dialog'

  click '.drop button'

  andThen ->
    assert.ok find('aside.hermes').hasClass('success'),
      'We see the success message'
    assert.equal currentPath(), 'userShows.index', 'We are back on the list'
    assert.equal find('.show').length, 0,
      'We don\'t see the show on the list anymore'
