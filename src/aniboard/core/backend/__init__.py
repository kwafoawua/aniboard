from .. import models, utils
import model_utils
import abc
import logging

LOGGER = logging.getLogger(__name__)
backend_apis = None


def load_backend_apis():
    global backend_apis

    if backend_apis is None:
        from . import hummingbird, mal, anilist
        backend_apis = {
            'hummingbird': hummingbird.API,
            'mal': mal.API,
            'anilist': anilist.API,
        }


def create_backend(backend_name, user):
    load_backend_apis()
    backend = None

    if backend_name in backend_apis:
        api_class = backend_apis[backend_name]
        backend = BackendFacade(backend_name, user, api_class)

    return backend


class BackendFacade(object):
    def __init__(self, name, user, api_class):
        self._name = name
        self._user = user

        LOGGER.debug('Setting up %s backend for %s', self._name, self._user)

        self._user_api_data = self._get_user_api_data()

        if self._user_api_data is None:
            self._api = api_class()
        else:
            self._api = api_class(**self._user_api_data.data)

    def _get_user_api_data(self, create=False):
        backend = models.Backend.objects.get(backend_name=self._name)
        user_api_data = None

        if create:
            user_api_data = models.UserAPIData.objects.get_or_create(
                backend=backend, user=self._user)[0]
        else:
            try:
                user_api_data = models.UserAPIData.objects.get(
                    backend=backend, user=self._user)
            except models.UserAPIData.DoesNotExist:
                pass

        return user_api_data

    def _get_show_api_data(self, user_show):
        show = user_show.show_source.show
        show_api_data = None

        try:
            show_api_data = show.api_data.get(backend__backend_name=self._name)
        except models.Backend.DoesNotExist:
            pass

        return show_api_data

    def _api_call(func):
        def wrapped(self, user_show):
            if not self._user_api_data:
                return

            show_api_data = self._get_show_api_data(user_show)

            if not show_api_data:
                return

            try:
                if self._api.authentication_expired:
                    # check expiration before trying anything, and if it
                    # has expired, refresh the authentication and update
                    # the stale user api data
                    new_data = self._api.refresh_authentication()
                    self._user_api_data.data.update(new_data)
                    self._user_api_data.save()

                # call the api method with the user show and it's api data
                func(self, user_show, show_api_data)
            except APIUnauthorizedException as e:
                # since it's unauthorized, delete the user api data
                self._user_api_data.delete()

                # email the user about it
                utils.send_mail(
                    '{} Integration'.format(self._user_api_data.backend.name),
                    'core/email/integration_disconnected.txt',
                    [self._user.email],
                )

                # re-raise the error as a normal exception (to avoid retry)
                raise Exception(e)

        return wrapped

    def authorize(self, **data):
        credentials = self._api.authenticate(**data)
        self._user_api_data = self._get_user_api_data(create=True)
        self._user_api_data.data.update(credentials)
        self._user_api_data.save()

    @_api_call
    def add_show(self, user_show, show_api_data):
        # apply the offset for the backend
        episodes_watched = user_show.last_episode_watched
        episodes_watched += show_api_data.episode_offset

        if episodes_watched == 0:
            status = BaseAPI.BASE_STATUS.PLANTOWATCH
        else:
            status = BaseAPI.BASE_STATUS.WATCHING

        self._api.add_show(
            episodes_watched=episodes_watched,
            status=status,
            id=show_api_data.api_id
        )

    @_api_call
    def push_show(self, user_show, show_api_data):
        if user_show.completed:
            status = BaseAPI.BASE_STATUS.COMPLETED
        else:
            status = BaseAPI.BASE_STATUS.WATCHING

        # apply the offset for the backend
        episodes_watched = user_show.last_episode_watched + \
            show_api_data.episode_offset

        self._api.update_show(
            episodes_watched=episodes_watched,
            rating=user_show.rating,
            status=status,
            id=show_api_data.api_id
        )

    @_api_call
    def drop_show(self, user_show, show_api_data):
        self._api.update_show(
            status=BaseAPI.BASE_STATUS.DROPPED,
            id=show_api_data.api_id
        )

    @_api_call
    def remove_show(self, user_show, show_api_data):
        self._api.remove_show(id=show_api_data.api_id)


class BaseAPI(object):
    __metaclass__ = abc.ABCMeta
    BASE_STATUS = model_utils.Choices(
        (1, 'WATCHING', 'Watching'),
        (2, 'COMPLETED', 'Completed'),
        (3, 'ONHOLD', 'On hold'),
        (4, 'DROPPED', 'Dropped'),
        (5, 'PLANTOWATCH', 'Plan to watch'),
    )

    @abc.abstractmethod
    def __init__(self, **data):
        pass

    @abc.abstractmethod
    def authenticate(self, **credentials):
        pass

    @property
    def authentication_expired(self):
        return False

    def refresh_authentication(self):
        pass

    @abc.abstractmethod
    def add_show(self, episodes_watched=None, status=None, id=None):
        pass

    @abc.abstractmethod
    def update_show(self, episodes_watched=None, rating=None, status=None,
                    id=None):
        pass

    @abc.abstractmethod
    def remove_show(self, id=None):
        pass

    @abc.abstractproperty
    def status_mapping(self):
        pass

    def get_status(self, base_status):
        return self.status_mapping[base_status] \
            if base_status in self.status_mapping else base_status


class APIException(Exception):
    pass


class APIUnauthorizedException(Exception):
    pass
