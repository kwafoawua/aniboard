`import Ember from 'ember'`

BackendConnectController = Ember.Controller.extend
  simpleAuth: Ember.computed 'model.authType', ->
    authType = @get 'model.authType'
    authType is 'simpleauth'

  actions:
    connect: (credentials) ->
      if not @get 'model.isConnecting'
        model = @get 'model'
        name = @get 'model.name'

        # try to connect
        @set 'hermes.info', "Connecting account to **#{name}**..."

        model.connect(credentials).then( =>
          @set 'hermes.success', "Successfully connected account to
          **#{name}**."

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', "Failed to connect account to **#{name}**."


`export default BackendConnectController`
