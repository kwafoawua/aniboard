import 'simple-auth-testing/test-helpers';
import './helpers/auth';
import './helpers/test-server-setup';
import './helpers/test-server-teardown';
import resolver from './helpers/resolver';
import {
  setResolver
} from 'ember-qunit';

setResolver(resolver);
